//
//  ViewController.swift
//  Kashish_TaskIt
//
//  Created by Kashish Goel on 2015-07-15.
//  Copyright (c) 2015 Kashish Goel. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation
import AVKit

@objc protocol AddTaskViewControllerDelegate {
    optional func addTask (message:String)
    optional func addTaskCanceled(message:String)
    optional func addDate(date:NSDate)
}

class AddTaskViewController: UIViewController,AVAudioPlayerDelegate{
    

    
    
    //@IBOutlet weak var dueDatePicker: UIDatePicker!
    @IBOutlet weak var dueDatePicker: UIDatePicker!
 
    var delegate = AddTaskViewControllerDelegate?()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
//       let background = UIImage(named: "background11.jpg")
       // dueDatePicker.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
       // dueDatePicker.backgroundColor = hexStringToUIColor("0F7386")
//                self.view.backgroundColor = UIColor(patternImage: background!)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    
//    @IBAction func cancelButtonTapped(sender: UIButton) {
//
//       
//     
//        
//    }
    
    @IBAction func cancelButtonTapped(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
        
    }
    
 
    
  
    func notificationCreater (date:NSDate, uuid:String) {
        
        for var notif = 0; notif < 60; notif++ {
            let date1 = date.dateByAddingTimeInterval(Double(notif))
            print(date1)
            let notification = UILocalNotification ()
            
            notification.alertBody = "If you're reading this, it's too late, wake up!"
            
            notification.fireDate = date1
            notification.repeatInterval = NSCalendarUnit.Minute
            //notification.userInfo = ["UUID": uuid]
            //  notification.soundName = "alarmSound.m4a"
            notification.alertAction = "Swipe to turn off Alarm"
            notification.soundName = "alarmSound.m4a"
            notification.alertTitle = "Test Title"
            
            print("schld")
            
            UIApplication.sharedApplication().scheduleLocalNotification(notification)
        }
    }
    
   override func  preferredStatusBarStyle() -> UIStatusBarStyle {
    return UIStatusBarStyle.LightContent
    
    }
    
   
    
    @IBAction func doneButtonPressed(sender: UIBarButtonItem) {
        
      

    }
    
    
    
    @IBAction func doneButton(sender: UIBarButtonItem) {
        let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        
        let managedObjectContext = appDelegate.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("TaskModel", inManagedObjectContext: managedObjectContext!)
        let task = TaskModel(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext!)
        
        //        task.task = taskTextField.text
        
        task.date = dueDatePicker.date
        task.uuid = NSUUID().UUIDString
        //if NSUserDefaults.standardUserDefaults().boolForKey(kShouldCompleteNewTodoKey) == true {task.completed = true}
        task.completed = false
        
        appDelegate.saveContext()
        
        
        let calendar = NSCalendar.currentCalendar()
        let comp = calendar.components(NSCalendarUnit.Second, fromDate: task.date)
        
        
        let seconds = Double(comp.second)
        //        let notification = UILocalNotification()
        //        notification.alertBody = "testBody"
        //        notification.fireDate = dueDatePicker.date
        //        notification.alertTitle = "testTitle"
        print("seconds:\(seconds)")
        
        //        let request = NSFetchRequest(entityName: "TaskModel")
        //
        //        var results:NSArray = managedObjectContext!.executeFetchRequest(request)
        notificationCreater(dueDatePicker.date, uuid: task.uuid)
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
