//
//  Date.swift
//  KashishGoel_TaskItApplicationForiOS
//
//  Created by Kashish Goel on 2015-07-15.
//  Copyright (c) 2015 Kashish Goel. All rights reserved.
//

import Foundation

class Date {
    
    class func from (year year:Int, month: Int, day: Int) -> NSDate {
        
        let components = NSDateComponents()
        components.year = year
        components.month = month
        components.day = day
        
        let gregorianCalendar = NSCalendar(identifier: NSGregorianCalendar)
        let date = gregorianCalendar?.dateFromComponents(components)
        
        return date!
    }
    
//    class func toString(date date:NSDate) -> String {
//        
////        let dateStringFormatter = NSDateFormatter()
////        dateStringFormatter.dateFormat = "yyyy-MM-dd"
////        let dateString = dateStringFormatter.stringFromDate(date)
////        let calendar = NSCalendar.currentCalendar()
////        let comp = calendar.components((NSCalendarUnit.Hour | NSCalendarUnit.Minute), fromDate: date)
////        let hour = comp.hour
////        let minute = comp.minute
////        
////        
////        return dateString
//    }
    
    
    
    
    
    
    
}
