//
//  TaskModel.swift
//  Photo Alarm
//
//  Created by Kashish Goel on 2015-08-18.
//  Copyright (c) 2015 Kashish Goel. All rights reserved.
//

import Foundation
import CoreData
@objc(TaskModel)
class TaskModel: NSManagedObject {

    @NSManaged var completed: NSNumber
    @NSManaged var date: NSDate
    @NSManaged var image: NSData
    
    @NSManaged var task: String
    @NSManaged var uuid: String

}


struct AlarmModel {
    var date:NSDate
}
