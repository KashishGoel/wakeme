//
//  ApiClass.h
//  TestApp
//
//  Created by Kashish Goel on 2015-08-11.
//  Copyright (c) 2015 Kashish Goel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@interface RequestViewController : UIViewController
extern NSString *tokenValue;
extern NSString *tokenString;

@property NSString* name;
- (void *) requestMethod: (UIImage *)imageToConvert;
//- (void *) responseMethod;


@end