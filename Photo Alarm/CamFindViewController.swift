//
//  MainViewController.swift
//  TestApp
//
//  Created by Kashish Goel on 2015-08-11.
//  Copyright (c) 2015 Kashish Goel. All rights reserved.
//

import UIKit
import CoreData
import MobileCoreServices
import Foundation

class MainViewController: RequestViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var tryAgainButton: UIBarButtonItem!
    @IBOutlet weak var serverTimeLabel: UILabel!
    var status:String?
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var camfindOutputLabel: UILabel!
    @IBOutlet weak var tryAgainLabel: UIButton!
    @IBOutlet weak var turnOffButton: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
      //  let background = UIImage(named: "background_summer.png")
    
//        self.view.backgroundColor = UIColor(patternImage: background!)

        let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("TaskModel", inManagedObjectContext: managedObjectContext!)
        let alarm = TaskModel(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext!)
        imageView.image = UIImage(data: alarm.image)
        // let string = tokenValue
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func cameraButtonPressed(sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
        let cameraController = UIImagePickerController()
            cameraController.delegate = self
            cameraController.sourceType = UIImagePickerControllerSourceType.Camera
            
         //   let mediaTypes:[AnyObject] = [kUTTypeImage]
            cameraController.mediaTypes = []
            cameraController.allowsEditing = false
            
            self.presentViewController(cameraController, animated: true, completion: nil)
        }
            
            
        else if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            
            let photoLibraryController = UIImagePickerController()
            photoLibraryController.delegate = self
            photoLibraryController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            
            
         //   let mediaTypes:[AnyObject] = [kUTTypeImage]
            photoLibraryController.mediaTypes = [kUTTypeImage as String]
            photoLibraryController.allowsEditing = false
            
            self.presentViewController(photoLibraryController, animated: true, completion: nil)
        }        else {
            let alertController = UIAlertController(title: "Alert", message: "Your device does not support the camera or photo Library", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }

    }
   
   
    @IBAction func tryAgainButtonPressed(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier("backToHome", sender: self)
    }
    
    @IBAction func saveButtonPressed(sender: UIButton) {
        
        
        //        let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        //        let entityDescription = NSEntityDescription.entityForName("TaskModel", inManagedObjectContext: managedObjectContext!)
        //        let alarm = TaskModel(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext!)
        requestMethod(imageView.image)
        sayHello()
        self.turnOffButton.hidden = true
        self.activityIndicator.hidden = false
        self.serverTimeLabel.hidden = false
        
        
        
        
        
        
    }
    
//    @IBAction func resultsButtonPressed(sender: UIButton) {
//        
//        var helloWorldTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("sayHello:"), userInfo: nil, repeats: false)
//        
//    }
    func sayHello()
    {
        let string1 = tokenValue as String
        var one = "https://camfind.p.mashape.com/image_responses"
        
        var url = "https://camfind.p.mashape.com/image_responses/"
        url += string1
        
        
        
        //            //writing
        //            text.writeToFile(path, atomically: false, encoding: NSUTF8StringEncoding, error: nil);
        //
        //            //reading
        //            let text2 = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)
        
        
        //              let string1 = tokenValue as String
        //        var one = "https://camfind.p.mashape.com/image_responses"
        let request : NSMutableURLRequest = NSMutableURLRequest()
        //          var url = "https://camfind.p.mashape.com/image_responses/"
        //        url += string1
        
        request.URL = NSURL(string: url)
        print("DAWG URL IS: \(url)")
        request.setValue("9hcyYCUJEsmsh4lNTgpgVX1xRq0Ip1uogovjsn5Mte0ONVBtes", forHTTPHeaderField: "X-Mashape-Key")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.HTTPMethod = "GET"
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse?, data: NSData?, error: NSError?) -> Void in
           // var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
             let jsonResult: NSDictionary!
            do { try jsonResult = NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers) as? NSDictionary
                if (jsonResult != nil) {
                    // process jsonResult
                    print(jsonResult)
                } else {
                    // couldn't load JSON, look at error
                }
                self.status = jsonResult["status"] as! String!
                
                
                //            println("yo dude status is : \(self.status!)")
                
                if self.status! == "404" {
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        //                    println("test")
                        self.sayHello()
                    }
                }
                    
                else if self.status! == "not completed" {
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        //                    println("test")
                        self.sendAnotherResponseRequest(url)
                    }
                    
                }
                    
                else if self.status == "completed" {
                    
                    self.performSegueWithIdentifier("backToHome", sender: self)
                }

            
            } catch {}
            
            
            
            
            
            
        })
    }
    
    
//    @IBAction func turnOffbuttonPressed(sender: UIButton) {
//        UIApplication.sharedApplication().cancelAllLocalNotifications()
//        let audio = (UIApplication.sharedApplication().delegate as! AppDelegate).backgroundMusic
//        if (audio?.playing != nil) {
//            audio?.stop()
//        }
//    }
    
    
    func sendAnotherResponseRequest(url:String) {
        let request : NSMutableURLRequest = NSMutableURLRequest()
        
        
        request.URL = NSURL(string: url)
        //  println("DAWG URL IS: \(url)")
        request.setValue("9hcyYCUJEsmsh4lNTgpgVX1xRq0Ip1uogovjsn5Mte0ONVBtes", forHTTPHeaderField: "X-Mashape-Key")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.HTTPMethod = "GET"
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse?, data: NSData?, error: NSError?) -> Void in
          //  var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            let jsonResult: NSDictionary!
            do {
                try  jsonResult = NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers) as? NSDictionary
                if (jsonResult != nil) {
                    // process jsonResult
                    print(jsonResult)
                } else {
                    // couldn't load JSON, look at error
                }
                let status = jsonResult["status"] as! String
                
                if status == "completed" {
                    let name = jsonResult["name"] as! String
                    if name.lowercaseString.rangeOfString("sink") != nil {
                        UIApplication.sharedApplication().cancelAllLocalNotifications()
                        let audio = (UIApplication.sharedApplication().delegate as! AppDelegate).backgroundMusic
                        if (audio?.playing != nil) {
                            audio?.stop()
                        }
                        
                        self.performSegueWithIdentifier("backToHome", sender: self)
                        print("done")
                        
                    }
                        
                    else if name.lowercaseString.rangeOfString("bathroom") != nil {
                        UIApplication.sharedApplication().cancelAllLocalNotifications()
                        let audio = (UIApplication.sharedApplication().delegate as! AppDelegate).backgroundMusic
                        if (audio?.playing != nil) {
                            audio?.stop()
                        }
                        self.performSegueWithIdentifier("backToHome", sender: self)
                        print("done")
                        
                    }
                    else if name.lowercaseString.rangeOfString("faucet") != nil {
                        UIApplication.sharedApplication().cancelAllLocalNotifications()
                        let audio = (UIApplication.sharedApplication().delegate as! AppDelegate).backgroundMusic
                        if (audio?.playing != nil) {
                            audio?.stop()
                        }
                        self.performSegueWithIdentifier("backToHome", sender: self)
                        print("done")
                        
                    }
                    else {
                        print("no match found")
                        dispatch_async(dispatch_get_main_queue()) {
                            self.errorLabel.hidden = false
                            self.camfindOutputLabel.text = name as String
                            self.camfindOutputLabel.hidden = false
                            
                            self.tryAgainButton.enabled = true
                        }
                        
                        print("done")
                        
                    }
                    
                    
                }
                else if status == "not completed" {
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        //                    println("test")
                        self.sendAnotherResponseRequest(url)
                    }
                }
                else if status == "timeout" {
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        //                    println("test")
                        self.tryAgainLabel.hidden = false
                        self.errorLabel.text = "Something went wrong on our side, try again."
                        
                    }
                }
            
            
            } catch {}
            
            
            
            
        })
        
        
        
    }
    
    
    //UIIMagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("TaskModel", inManagedObjectContext: managedObjectContext!)
        let alarm = TaskModel(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext!)
        alarm.image = imageData!
        imageView.image = UIImage(data: alarm.image)
        turnOffButton.hidden = false
        
        
        (UIApplication.sharedApplication().delegate as! AppDelegate).saveContext()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
